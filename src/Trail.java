public class Trail {
    private int[] markers;

    public Trail(int[] markers) {
        this.markers = markers;
    }

    public boolean isLevelTrailSegment(int start, int end) {
        int min = this.markers[start];
        int max = min;

        for (int i = start; i < end; i++) {
            min = this.markers[i] < min ? this.markers[i] : min;
            max = this.markers[i] > max ? this.markers[i] : max;
        }
        System.out.println(min + " " + max);
        return Math.abs(max - min) <= 10;
    }

    public boolean isDifficult() {
        int n = 0;
        for (int i = 1; i < this.markers.length; i++) {
            if (Math.abs(this.markers[i] - this.markers[i - 1]) >= 30)
                n = n + 1;
        }
        return n >= 3;
    }
}
